container=tlc-wordpress-1

docker exec -it $container rm -rf /var/www/html/wp-content/plugins/tlc-intra-events/
docker cp wordpress/. $container:/var/www/html/wp-content/plugins/tlc-intra-events/
docker cp build/static/. $container:/var/www/html/wp-content/plugins/tlc-intra-events/static/
