<?php
/*
 * Plugin Name: TLC Intra events
 * Plugin URI:
 * Description: TLC event calendar using data from Intranet
 * Uses React widget to render the calendar for user.
 *
 * Version: 0.0.6
 * Author: Joonatan Kuosa
 */

add_action( 'init', function () {
  add_shortcode('tlc_intra_events', 'tlc_intra_events');
} );

// TODO translate the error message
function tlc_intra_events( $params ) {

  $small = false;
  if ($params && array_key_exists('small', $params)) {
    $small = $params['small'];
  }

  ob_start();
?>
  <body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div
      id="tlc-intra-events"
      class="tlc-intra-events <?= $small ? "tlc-intra-events-small" : "tlc-intra-events-large"; ?>">
    </div>
  </body>
<?php
  return ob_get_clean();
}

/* enque React App */
add_action( 'wp_enqueue_scripts', 'tlc_intra_events_enqueue_scripts', 1 );
function tlc_intra_events_enqueue_scripts() {
  // find the CSS and JS bundles
  $fcss = glob(__DIR__ . "/static/css/main.*.css");
  $fjs = glob(__DIR__ . "/static/js/main.*.js");

  if ( count($fcss) > 0 ) {
    $spos = strrpos($fcss[0], "main.");
    $file_str = substr($fcss[0], $spos);
    error_log('CSS file url: ' . plugins_url( "/static/css/{$file_str}", __FILE__ ));

    wp_enqueue_style(
      'tlc-intra-events-react-style',
      plugins_url( "/static/css/{$file_str}", __FILE__ )
    );

  } else {
    error_log("ERROR: tlc-events-intra CSS files missing ");
  }

  if ( count($fjs) > 0 ) {
    $spos = strrpos($fjs[0], "main.");
    $file_str = substr($fjs[0], $spos);
    error_log('JS file url: ' . plugins_url( "/static/js/{$file_str}", __FILE__ ));

    wp_register_script(
      'tlc-intra-events-js',
      plugins_url( "/static/js/{$file_str}", __FILE__ ),
      [],
      false,
      true
    );

    wp_localize_script( 'tlc-intra-events-js', 'DATA', [
      'url' => get_site_url(),
      'lang' => function_exists( 'pll_current_language' ) ? pll_current_language() : ""
    ] );
    wp_enqueue_script( 'tlc-intra-events-js' );
  } else {
    error_log("ERROR: tlc-intra-events JS files missing ");
  }
}

/******************************* REST ****************************************/
// Proxy the REST request to bypass CORS and hide authorization from userspace
add_action( 'rest_api_init', function () {
  register_rest_route( 'tlc/v1', '/events', array(
    array (
      'methods' => WP_REST_Server::READABLE,
      'callback' => 'tlc_intra_get_from_rest',
      'permission_callback' => '__return_true',
    )
  ) );
} );

// query params: ?lang=en to switch to english otherwise finnish events
function tlc_intra_get_from_rest (WP_REST_Request $request) {
  $lang = $request->get_param( 'lang' );

  //$url = substr($lang, 2) === 'en'
  $url = $lang === 'en'
    ? get_option('tlc_intra_events_url_en')
    : get_option('tlc_intra_events_url_fi');
  $user = get_option('tlc_intra_events_user');
  $password = get_option('tlc_intra_events_pass');

  $opts = array('http' =>
    array(
      'method'  => 'GET',
      'header'  => "Content-Type: text/xml\r\n".
      "Authorization: Basic " . base64_encode("$user:$password")."\r\n",
      'timeout' => 60
    )
  );

  $context = stream_context_create($opts);
  $response = file_get_contents($url, false, $context);
  // need to decode otherwise we get double encoding when returning it
  $json = json_decode($response);
  // The JSON structure
  // [{ ..., nid, "link": "http://intra.tlc.fi/..."}, ...]

  if ($response === false) {
    return new WP_Error( 'tlc_intra: get events', __( 'Get events from intra API failed.', 'tlc_intra' ) );
  } else {
    // modify the JSON to include custom link overrides
    global $wpdb;
    $table_name = $wpdb->prefix . 'tlc_intra_events_links';
    $results = $wpdb->get_results( "SELECT * FROM $table_name" );
    foreach ($results as $row) {
      $json = array_map(function($event) use ($row) {
        if ($event->nid === $row->event_id) {
          $event->link = $row->url;
        }
        return $event;
      }, $json);
    }
    return rest_ensure_response( $json );
  }
}

/**
 * Register the /wp-json/tlc/v1/events endpoint so it will be cached.
 */
function tlc_intra_events_add_cache_rest_endpoint( $allowed_endpoints ) {
  if ( ! isset( $allowed_endpoints[ 'tlc/v1' ] ) || ! in_array( 'events', $allowed_endpoints[ 'tlc/v1' ] ) ) {
    $allowed_endpoints[ 'tlc/v1' ][] = 'events';
  }
  return $allowed_endpoints;
}
add_filter( 'wp_rest_cache/allowed_endpoints', 'tlc_intra_events_add_cache_rest_endpoint', 10, 1);

/******************************* Admin ***************************************/
add_action( 'admin_menu', function () {
  add_menu_page(
    __( 'TLC intra events', 'tlc-intra-events' ),
    __( 'TLC intra events', 'tlc-intra-events' ),
    'edit_posts',
    'tlc-intra-events',
    'tlc_intra_events_settings',
    'dashicons-groups',
    6
  );
} );

function tlc_intra_events_settings_init() {
  add_settings_section(
    'tlc_intra_events_setting_section',
    'TLC Intra events importer settings',
    'tlc_intra_events_setting_section_callback_function',
    'tlc_intra_events'
  );

  add_settings_field(
    'tlc_intra_events_setting_url_fi',
    'URL FI',
    'tlc_intra_events_url_fi_setting_callback_function',
    'tlc_intra_events',
    'tlc_intra_events_setting_section'
  );

  add_settings_field(
    'tlc_intra_events_setting_url_en',
    'URL EN',
    'tlc_intra_events_url_en_setting_callback_function',
    'tlc_intra_events',
    'tlc_intra_events_setting_section'
  );

  add_settings_field(
    'tlc_intra_events_setting_user',
    'User',
    'tlc_intra_events_user_setting_callback_function',
    'tlc_intra_events',
    'tlc_intra_events_setting_section'
  );

  add_settings_field(
    'tlc_intra_events_setting_password',
    'Password',
    'tlc_intra_events_pass_setting_callback_function',
    'tlc_intra_events',
    'tlc_intra_events_setting_section'
  );

  register_setting( 'tlc_intra_events', 'tlc_intra_events_url_fi');
  register_setting( 'tlc_intra_events', 'tlc_intra_events_url_en');
  register_setting( 'tlc_intra_events', 'tlc_intra_events_user');
  register_setting( 'tlc_intra_events', 'tlc_intra_events_pass');
}
add_action( 'admin_init', 'tlc_intra_events_settings_init' );

function tlc_intra_events_setting_section_callback_function() {
  echo '<p>The JSON API address for intra events and basic authentication user and password for it.</p>';
}

function tlc_intra_events_user_setting_callback_function() {
?>
  <input style="width: 100%; max-width: 600px;" id="tlc_settings_user" name="tlc_intra_events_user" type="text" value="<?php echo get_option('tlc_intra_events_user'); ?>"></input>
<?php
}
function tlc_intra_events_pass_setting_callback_function() {
?>
  <input style="width: 100%; max-width: 600px;" id="tlc_settings_pass" name="tlc_intra_events_pass" type="password" value="<?php echo get_option('tlc_intra_events_pass'); ?>"></input>
<?php
}
function tlc_intra_events_url_fi_setting_callback_function() {
?>
  <input style="width: 100%; max-width: 600px;" id="tlc_settings_url_fi" name="tlc_intra_events_url_fi" type="url" value="<?php echo get_option('tlc_intra_events_url_fi'); ?>"></input>
<?php
}

function tlc_intra_events_url_en_setting_callback_function() {
?>
  <input style="width: 100%; max-width: 600px;" id="tlc_settings_url_en" name="tlc_intra_events_url_en" type="url" value="<?php echo get_option('tlc_intra_events_url_en'); ?>"></input>
<?php
}

// Render settings page
function tlc_intra_events_settings () {
?>
  <form method="POST" action="options.php">
  <?php
    settings_fields( 'tlc_intra_events' );
    do_settings_sections( 'tlc_intra_events' );
    submit_button();
  ?>
  </form>
<?php
}

// Database for overrides (links for now)
function tlc_intra_events_install() {
  global $wpdb;

  $table_name = $wpdb->prefix . 'tlc_intra_events_links';

  $charset_collate = $wpdb->get_charset_collate();

  // link length is based on Chrome's max URL length (the shortest of major browsers)
  $sql = "CREATE TABLE $table_name (
      event_id INT NOT NULL,
      url VARCHAR(2083) NOT NULL DEFAULT '',
      PRIMARY KEY  (event_id)
  ) $charset_collate;";

  require_once ABSPATH . 'wp-admin/includes/upgrade.php';
  dbDelta( $sql );
}

register_activation_hook( __FILE__, 'tlc_intra_events_install' );
