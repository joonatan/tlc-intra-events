# TLC intra events plugin

An event calendar plugin for [TLC website](https://www.tuni.fi/tlc/)
designed to work with the [WP theme](https://gitlab.com/joonatan/tlc).

Adds a Calendar widget that shows events ment for a certain user group (in this case university teachers),
with a Modal to show more detailed information about the event.

Can be used in small spaces (mobile / sidebar widget) to show only an N events on a specific day, and on
desktop to show the event names also. Modal includes all the events for that day by name,
allowing user to open them for the description per day.

## Description

Adds a calendar widget (made with React) to the frontend.

Adds a REST endpoint for passthrough intra events from University API endpoint.
The custom endpoint is added for caching purposes, since without it the calendar is extremely slow,
with locally cached events even though the events are loaded on the frontend they are rather fast.
Also handles issues with CORS and authentication.

Has WP admin settings for the intra events endpoint and authentication.

Supports separate events for multiple languages (Finnish and English).

Has a custom link override system that has no admin frontend (only through direct database changes).

## Tech

- React 18
- php 8
- WordPress 6+ (tested on 6.6 and 6.7)

## Requirements

NPM dependencies

- React 18
- TailwindCSS
- React Calendar
- react-i18next
- axios
