import axios from 'axios'

import { addDays, parseRrule } from './utility.js'

// window.DATA for url so this works with multi-site
const instance = axios.create({
  baseURL: (window.DATA ? window.DATA.url : '') +
    process.env.REACT_APP_REST_API_URL,
})

export const getEvents = (lang) => (
  instance.get('/events', { params: { lang: lang } }).then((res) => (
    res.data.map((x) => ({
      title: x.title,
      category: x.field_event_category,
      reg_time: x.field_registration_time ? new Date(x.field_registration_time) : null,
      times: x.field_event_date.map((y) => {
        // fix the APIs incorrect date strings "2022-04-9T" instead of "2022-04-09T"
        const re = /(\d*)-(\d+)-(\d+)T(\d+):(\d+):(\d+)/i
        const s = y.value.match(re)
        const e = y.end_value.match(re)

        return {
          start: new Date(Date.UTC(s[1], s[2]-1, s[3], s[4], s[5])),
          end: new Date(Date.UTC(e[1], e[2]-1, e[3], e[4], e[5])),
          rrule: parseRrule(y.rrule),
        }
      }).reduce((arr, y) => {
        if (y.rrule && (y.rrule.until - y.start) > 0) {
          const weeks = Math.floor((y.rrule.until - y.start) / (1000 * 60 * 60 * 24 * 7))
          // byday defines the weekday for repeating event
          const diffWeekday = (y.start.getDay() - 1) - y.rrule.byday
          const expanded = [...Array(weeks).keys()]
            .map((z, index) => ({
              start: addDays(new Date(y.start), (index + 1) * 7 - diffWeekday),
              end: addDays(new Date(y.end), (index + 1) * 7 - diffWeekday),
            }))

          return [
            ...arr,
            ...expanded,
            { start: y.start, end: y.end },
          ]
        } else {
          return [...arr, { start: y.start, end: y.end }]
        }
      }, []),
      ingress: x.field_ingress,
      link: x.link,
      nid: x.nid,
    }))
  ))
)

export default instance
