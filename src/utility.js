export const dateTimeToString = (d, locale = 'fi') =>
  d.toLocaleString(locale, { timeStyle: 'short' })

export const dateToString = (d, locale) => {
  const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
  return d.toLocaleString(locale, options)
}

export const addDays = (date, days) => {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate() + days, date.getHours(), date.getMinutes())
}

//"rrule": "FREQ=WEEKLY;INTERVAL=1;BYDAY=WE;UNTIL=20220427T120000Z",
export const parseRrule = (str) => {
  const convertByDay = (byday) => {
    switch (byday) {
      case 'MO':
        return 0
      case 'TU':
        return 1
      case 'WE':
        return 2
      case 'TH':
        return 3
      case 'FR':
        return 4
      case 'SA':
        return 5
      case 'SU':
        return 6
      default:
        return 0
    }
  }

  if (!str) {
    return null
  } else {
    // The rrule string changes so fix it
    const rrules = str.replace('RRULE:','').split(';').map((x)=>x.split('='))

    // parse date
    const convertTimeString = (timeStr) => (
      new Date(
        Date.UTC(
          +timeStr.substring(0, 4),
          +timeStr.substring(4, 6)-1,
          +timeStr.substring(6, 8),
          +timeStr.substring(9, 11),
          +timeStr.substring(11, 13),
          +timeStr.substring(13, 15)
        )
      )
    )

    // parse rules
    return rrules.reduce((val, x) => {
      switch (x[0]) {
        case 'FREQ':
          return { ...val, freq: x[1] }
        case 'INTERVAL':
          return { ...val, interval: +x[1] }
        case 'BYDAY':
          return { ...val, byday: convertByDay(x[1]) }
        case 'UNTIL':
          return { ...val, until: convertTimeString(x[1]) }
        default:
          return val
      }
    }, {})
  }
}
