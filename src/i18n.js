import i18n from "i18next"
import { initReactI18next } from "react-i18next"
import Backend from 'i18next-http-backend'
import LanguageDetector from 'i18next-browser-languagedetector'

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources: {
      en: {
        translation: {
          "days": "days",
          "hours": "hours",
          "minutes": "minutes",
          "Register in": "Register in",
          "Starts in": "Starts in",
          "Event is over.": "Event is over.",
          "Registering is over.": "Registering is over.",
          "Events": "Events",
          "close": "close",
        },
      },
      fi: {
        translation: {
          "days": "päivää",
          "hours": "tuntia",
          "minutes": "minuuttia",
          "Register in": "Ilmottaudu",
          "Starts in": "Alkaa",
          "Event is over.": "Tapahtuma on jo ohi.",
          "Registering is over.": "Rekisteröinti on jo ohi.",
          "Events": "Tapahtumat",
          "close": "sulje",
          'Loading events. Please wait a moment.': 'Lataa tapahtumia, odota hetki.'
        },
      },
    },
    fallbackLng: 'en',
    interpolation: {
      escapeValue: false // react already safes from xss
    }
  })

export default i18n
