import React from 'react'

import EventView from './EventView'

// Show a singular event directly, multiple events as a list with accordian
const EventList = ({ events }) => {
  const [selected, setSelected] = React.useState(-1)

  return (
    <div className="p-0">
      {events.map((x, i, arr) => (
        <EventView
          key={x.key}
          isOpen={arr.length < 2 || selected === i}
          className="relative max-w-screen-md p-3 mb-6 bg-gray-100 shadow-md shadow-gray-500"
          event={x}
          onOpen={() => setSelected(i)}
        />
      ))}
    </div>
  )
}

export default EventList
