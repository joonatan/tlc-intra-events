import React from 'react'

import { useTranslation } from 'react-i18next'

import { dateTimeToString } from '../utility.js'

const TimeToEvent = ({ start_time, type }) => {
  const { t } = useTranslation()

  if (new Date() < start_time) {
    const diff = start_time - new Date()
    const total_mins = diff / 1000 / 60
    const days = Math.floor(total_mins / 60 / 24)
    const hours = Math.floor((total_mins - days*60*24) / 60)
    const minutes = Math.floor(total_mins - days * 24 * 60 - hours * 60)
    return (
      <div className="text-sm mb-4 text-green-600">
        {type === 'register' ? t('Register in') : t('Starts in')} {days > 0 ? `${days} ${t('days')} ` : ''}
        {hours} {t('hours')} {minutes} {t('minutes')}
      </div>
    )
  } else {
    return <div className="text-sm mb-4 text-red-600">
      {type === 'register' ? t('Registering is over.') : t('Event is over.')}
    </div>
  }
}

/* TODO add a marker for events that are repeating
 */
const Inner = ({ isOpen, className, event }) => {

  // find the next occurance of the event
  const nextEvents = event.times.filter((x) => x.start > new Date())

  return (
    <div className={`${className} w-full`}>
      <div className="flex w-full md:max-w-85p">
        <h3 className="mt-12 md:mt-0 text-xl text-slate-800 font-semibold p-0 mb-4">
          {isOpen ? (
            <span className="leading-none text-primary mr-8">&#9650;</span>
          ) : (
            <span className="leading-none text-primary mr-8">&#9660;</span>
          )}
          <span dangerouslySetInnerHTML={{__html: event.title}} />
        </h3>
        {event.times.length > 0 && (
          <div
            key={event.times[0].start.toString()}
            className="text-sm text-white bg-primary absolute top-0 right-0 py-2 px-4 rounded-bl-lg"
          >
            {dateTimeToString(event.times[0].start)} - {dateTimeToString(event.times[0].end)}
          </div>
        )}
      </div>
      {isOpen && (
      <div>
        <p className="text-md mb-8"
           dangerouslySetInnerHTML={{__html: event.ingress }}
        />
        <p className="text-md">
          <a
            className="text-primary underline"
            href={event.link}
            rel="noreferrer noopener"
            target="_blank">
            {event.link}
          </a>
        </p>
      </div>
      )}
      <div className="flex flex-col md:flex-row justify-between mt-4">
        <TimeToEvent
          start_time={nextEvents.length > 0 ? nextEvents[0].start : event.times[0].start}
          type='event'
        />
        {event.reg_time && (
          <TimeToEvent start_time={event.reg_time} type='register' />
        )}
      </div>
    </div>
  )
}

// Event contains only the sepecfic date (event.times is either 0 or 1 length)
const EventView = ({ className, event, isOpen, onOpen }) => {

  // TODO add aria to the button
  return !isOpen ? (
    <button
      style={{ display: 'block', textAlign: 'left', width: '100%' }}
      type="button"
      onClick={() => onOpen()}>
      <Inner
        event={event}
        isOpen={isOpen}
        className={className}
      />
    </button>
  ) : (
      <Inner
        event={event}
        isOpen={isOpen}
        className={className}
      />
  )
}

export default EventView
