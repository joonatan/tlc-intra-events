import React from 'react'
import Calendar from 'react-calendar'
import 'react-calendar/dist/Calendar.css'
import { useTranslation } from 'react-i18next'
import Modal from 'react-modal'

import EventList from './components/EventList'
import './App.css'
import { dateTimeToString, dateToString } from './utility.js'
import { getEvents } from './api.js'

const modalStyle = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: 'min(750px, 95vw)',
  },
}

const smallModalStyle = {
  content: {
    inset: '10px',
    padding: '20px 10px',
  },
  overlay: {
    overflowY: "auto",
  }
}

const LARGE_SIZE_CUTOFF = 800

Modal.setAppElement('#tlc-intra-events')

const isDivSmall = () => {
  if (window.innerWidth < LARGE_SIZE_CUTOFF) {
    return true
  }

  const e = window.document.getElementById('tlc-intra-events')
  return Array.from(e.classList).filter((x) => x === 'tlc-intra-events-small').length > 0
}

function App() {
  const [events, setEvents] = React.useState([])
  const [date, setDate] = React.useState(new Date())
  const [isModalOpen, setIsModalOpen] = React.useState(false)
  const [isWindowSmall, setWindowIsSmall] = React.useState(window.innerWidth < LARGE_SIZE_CUTOFF)
  const [selectedMonth, setSelectedMonth] = React.useState((new Date()).getMonth())

  const handleResize = () => {
    setWindowIsSmall(window.innerWidth < LARGE_SIZE_CUTOFF)
  }

  React.useEffect(() => {
    window.addEventListener("resize", handleResize)
    return(() => {
        window.removeEventListener('resize', handleResize)
    })
  }, [])

  const { i18n, t } = useTranslation()

  const changeDate = (d) => {
    setDate(d)
    setIsModalOpen(true)
  }

  const handleViewChange = ({ active, activeStartDate, value, view }) => {
    if (view === 'month') {
      // filter events by month
      setSelectedMonth(activeStartDate.getMonth())
    }
  }

  // use the language passed by WordPress
  React.useEffect(() => {
    const lang = window.DATA ? window.DATA.lang : ""
    if (lang !== "") {
      i18n.changeLanguage(lang)
    }
  }, [i18n, i18n.changeLanguage])

  React.useEffect(() => {
    const lang = i18n.language
    getEvents(lang)
      .then((res) => setEvents(res))
      .catch((err) => {
        console.log('error: ', err)
        // TODO if we have an error show a widget with an error message instead (don't alert the user)
        // but don't show the calendar widget either
        // should be a widget the same size as the calendar but only showing an error message
      })
  }, [i18n.language])

  const isDateEqual = (a, b) => (
    a.getFullYear() === b.getFullYear() &&
    a.getMonth() === b.getMonth() &&
    a.getDate() === b.getDate()
  )

  const fEvents = (date) => (
    events
    // filter doesn't work properly for event at 1.6
    //.filter((x) => x.times[0].start.getMonth() === selectedMonth)
    .filter((x) => x.times.map(
        (y) => isDateEqual(y.start, date)
      ).some((a) => a)
    )
    .map((x) => ({
      ...x,
      times: x.times.filter((a) => isDateEqual(a.start, date)),
    }))
    .map((x) => ({
      ...x,
      key: x.title + x.times[0].start,
    }))
    .sort((a, b) => a.times[0].start - b.times[0].start)
  )

  // Add markers for days with events
  const addEventMarkers = (labelParam) => {
    if (labelParam.view !== "month") {
      return
    }

    const f_events = fEvents(labelParam.date)

    return f_events.length > 0 ? (
      <div className="inline-flex flex-col">
        {f_events.map((x) => (
          <span key={x.key} className="bg-primary w-1 p-2 rounded-full inline-block ml-2 mt-2"></span>
        ))}
      </div>
    ) : null
  }

  const addEventLabels = (labelParam) => {
    if (labelParam.view !== "month") {
      return
    }

    const snupTitles = (title, max_length) => (
      title.substring(0, max_length) + (title.length > max_length ? '...' : '')
    )

    const f_events = fEvents(labelParam.date)

    return f_events.length > 0 ? (
      <div className="mt-3 text-left">
        {f_events.map((x) => (
          <div key={x.key} className="text-sm mb-3 font-normal">
            {dateTimeToString(x.times[0].start)}
            {' '}
            <span dangerouslySetInnerHTML={{ __html: snupTitles(x.title, 64) }} />
          </div>
        ))}
      </div>
    ) : null
  }

  const closeModal = () => setIsModalOpen(false)

  // TODO filter events based on month here (slice only the mouths that are shown)
  //  (this makes the tiling faster)
  //  needs to have also one previous week and one next week
  return (
    <div className="mb-8">
      {events.length === 0 && <div>{t('Loading events. Please wait a moment.')}</div>}
      <Calendar
        value={date}
        onChange={changeDate}
        onActiveStartDateChange={handleViewChange}
        tileContent={isDivSmall() || isWindowSmall ? addEventMarkers : addEventLabels}
        tileDisabled={({ a, date, view }) => fEvents(date).length === 0}
        locale={i18n.language}
        minDetail="month"
        prev2Label={null}
        next2Label={null}
      />
      <Modal
        isOpen={isModalOpen}
        contentLabel={t('Events') + ' ' + dateToString(date, i18n.language)}
        onRequestClose={closeModal}
        style={isWindowSmall ? smallModalStyle : modalStyle}
        preventScroll={false}
        className="p-4 bg-gray-200 rounded-lg shadow-lg absolute overflow-y-auto max-h-modal "
        overlayClassName="z-100 py-10 fixed inset-0 bg-black bg-opacity-50"
      >
        <button
          className="absolute top-0 right-0 mb-4 mr-4 text-black text-2xl p-2 border-none bg-inherit"
          onClick={closeModal}
          aria-label={t('close')}
        >
          X
        </button>
        <h2 className="text-2xl mt-0 mr-8 text-center text-primary">{t('Events')}</h2>
        <p className="text-md mb-8 text-center">{dateToString(date, i18n.language)}</p>
        <EventList events={fEvents(date)} />
      </Modal>
    </div>
  )
}

export default App
