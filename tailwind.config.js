module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#4e008e',
        'secondary': '#c3b9d7',
      },
      maxWidth: {
        '85p': '85%',
      },
      maxHeight: {
        'modal': '90%',
      },
    },
    zIndex: {
      100: 100,
    },
    fontSize: {
      'xs': '1.2rem',
      'sm': '1.4rem',
      'md': '1.6rem',
      'base': '1.6rem',
      'lg': '2.0rem',
      'xl': '2.2rem',
      '2xl': '2.5rem',
      '3xl': '3.125rem',
      '4xl': '3.625rem',
    },
  },
  plugins: [
    //require("@tailwindcss/typography"),
  ],
}
